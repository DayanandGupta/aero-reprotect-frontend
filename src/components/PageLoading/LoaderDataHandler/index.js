import React from 'react'
import { Empty } from 'antd';
import { LoaderDataHandlerType } from '@/types/component.proptypes';
import PageLoading from '../index';

function LoaderDataHandler({loading}) {
  return (loading ?  <PageLoading /> : <div data-test="empty-component"><Empty /></div> )
}

LoaderDataHandler.defaultProps = {
  loading: true
};

LoaderDataHandler.propTypes = LoaderDataHandlerType;

export default LoaderDataHandler;
