# FROM node:12 as build-stage

# WORKDIR /app

# COPY ./package.json /app/

# RUN npm install

# COPY ./ /app/

# RUN npm run build

# FROM nginx:stable

# COPY --from=build-stage /app/dist/ /usr/share/nginx/html

# COPY ./tools/docker/nginx/nginx.conf /etc/nginx/conf.d/nginx.conf.template

# # CMD /bin/bash -c "envsubst '\${NGINX_PORT} \${DEFINITION_URL} \${PROMOTION_URL} \${IMAGE_URL} \${RULE_URL} \${MAX_IMAGE_BODY_SIZE}' < /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/conf.d/default.conf && cat /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"
# CMD /bin/bash -c "envsubst '\${NGINX_PORT} \${MANAGER_URL} \${MAX_IMAGE_BODY_SIZE}' < /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/conf.d/default.conf && cat /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"

# FROM node:12 as build-stage

# WORKDIR /app

# COPY ./package.json /app/

# RUN npm install

# COPY ./ /app/

# RUN npm run build

FROM nginx:stable

COPY  ./dist/ /usr/share/nginx/html

COPY ./tools/docker/nginx/nginx.conf /etc/nginx/conf.d/nginx.conf.template

# CMD /bin/bash -c "envsubst '\${NGINX_PORT} \${DEFINITION_URL} \${PROMOTION_URL} \${IMAGE_URL} \${RULE_URL} \${MAX_IMAGE_BODY_SIZE}' < /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/conf.d/default.conf && cat /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"
CMD /bin/bash -c "envsubst '\${NGINX_PORT} \${MANAGER_URL} \${MAX_IMAGE_BODY_SIZE}' < /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/conf.d/default.conf && cat /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"
